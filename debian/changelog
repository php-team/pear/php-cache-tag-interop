php-cache-tag-interop (1.1.0-3) unstable; urgency=medium

  * Update standards version to 4.6.2
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Thu, 07 Mar 2024 12:21:04 +0100

php-cache-tag-interop (1.1.0-2) unstable; urgency=medium

  * Mark package as Multi-Arch: foreign
  * Update debian/watch URL
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Thu, 22 Sep 2022 07:50:16 +0200

php-cache-tag-interop (1.1.0-1) unstable; urgency=medium

  [ David Prévot ]
  * Fix d/watch after hosting change
  * Simplify gbp import-orig
  * Generate phpabtpl at build time
  * Install dh-sequence-* instead of using dh --with
  * Update standards version to 4.6.0, no changes needed.

  [ Ruud Kamphuis ]
  * Allow psr/log ^1.0 || ^2.0 || ^3.0 (#254)

 -- David Prévot <taffit@debian.org>  Fri, 31 Dec 2021 12:27:34 -0400

php-cache-tag-interop (1.0.1-1) unstable; urgency=medium

  [ Tobias Nyholm ]
  * Show that we support PHP8 (#238)

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * d/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update watch file format version to 4.
    + Set Rules-Requires-Root: no.
    + Update Standards-Version to 4.5.1
    + Use debhelper-compat 13
  * d/upstream/metadat:
    + Set fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.
    + Remove obsolete fields Contact, Name (already present in
      machine-readable debian/copyright).

 -- David Prévot <taffit@debian.org>  Sun, 06 Dec 2020 08:47:25 -0400

php-cache-tag-interop (1.0.0-2) unstable; urgency=medium

  * Use secure URI in Homepage field.
  * Update standards version, no changes needed.
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Contact, Name.
  * Drop get-orig-source target
  * Move repository to salsa.d.o
  * Update Standards-Version to 4.4.0

 -- David Prévot <taffit@debian.org>  Thu, 22 Aug 2019 15:35:01 -1000

php-cache-tag-interop (1.0.0-1) unstable; urgency=low

  * Initial release (new php-cache-integration-tests dependency)

 -- David Prévot <taffit@debian.org>  Sun, 17 Sep 2017 10:00:13 -1000
